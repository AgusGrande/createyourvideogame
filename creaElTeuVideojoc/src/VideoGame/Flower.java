package VideoGame;

import Core.*;

public class Flower extends PowerUps{

	public Flower(int x1, int y1, int x2, int y2, Field f) {
		super("flower", x1, y1, x2, y2, "resources/misc/flower.png", f, PowerUpTipus.FLOWER);
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
	

}
