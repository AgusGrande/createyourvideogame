package Part4;

import Core.*;
import Core.Window;
import Part4.*;

public class main {

	static Field fie = new Field();
	static Window w3 = new Window(fie);

	public static void main(String[] args) throws InterruptedException {

		fie.background="resources/Solid_black.png";

		Projectil projectil1=new Projectil("disparo",0, 0, 3, 3,"resources/Bullet.png",fie);
		Nau nave = new Nau(fie, 1000,1000, 1100,1050,"resources/Ship.png",projectil1);
		
		
		EnemicBasic elEnemigo = new EnemicBasic(fie, 1000,25,1100,100,"resources/InvaderB1.png",1);
		Spawner spawner=new Spawner(fie, 2000);
		
		Puntuacio puntuacio=new Puntuacio("nom", 100,25,1000,100, 0, "PROVA", fie);

		boolean sortir = false;
		while (!sortir) {
			input(nave);
			fie.draw();
			Thread.sleep(10);
		}

	}
	
	static public void input(Nau character) {
		
	
		if(w3.getPressedKeys().contains('d')) {
			character.moviment(Input.DRETA);
		} else if(w3.getPressedKeys().contains('a')) {
			character.moviment(Input.ESQUERRA);
		}
		
		if(w3.getKeysUp().contains('d')) {
			character.treuremoviment(Input.DRETA);
		} else if(w3.getKeysUp().contains('a')) {
			character.treuremoviment(Input.ESQUERRA);
		}
		
		if(w3.getKeysDown().contains(' ')) {
			character.disparar();
		}

	}


}
