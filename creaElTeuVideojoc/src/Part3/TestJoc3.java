package Part3;


import Core.*;

public class TestJoc3 {
	static Field f3 = new Field();
	static Field f2 = new Field();
	static Window w3 = new Window(f3);

	public static void main(String[] args) throws InterruptedException {

		f3.background="resources/bg.gif";
		f2.background="resources/suelo.jpg";
		

		Roca roca = new Roca("Roca", 0, 800, 700, 1300, 0, "resources/suelo.jpg", f3 );
		Roca roca1 = new Roca("Roca", 1100, 800, 2000, 1300, 0, "resources/suelo.jpg", f3 );
		
		Obstacle rompible = new Obstacle("RocaRompible", 400, 100, 700, 800, 0, "resources/sueloR.jpg", f3 );
		
		Pinchos pinchos = new Pinchos("Spikes", 700, 900, 1100, 1100, 0, "resources/spikes.png", f3 );
		
		Projectil projectil1=new Projectil("disparo",0, 0, 3, 3,"resources/skulll.gif",f3);
		Personatge elPersonaje = new Personatge(f3, 200, 300, 150,"resources/terraria.gif",projectil1);
		
		
		Enemic elEnemigo = new Enemic(f3, 1090, 650, 150,"resources/creatureS.png",2);

		boolean sortir = false;
		while (!sortir) {
			input(elPersonaje);
			f3.draw();
			Thread.sleep(10);
		}

	}
	
	static public void input(Personatge character) {
		
		if(w3.getKeysDown().contains('w')) {
			character.moviment(Input.AMUNT);
		}
		
		if(w3.getPressedKeys().contains('d')) {
			character.moviment(Input.DRETA);
		} else if(w3.getPressedKeys().contains('a')) {
			character.moviment(Input.ESQUERRA);
		}
		
		if(w3.getKeysUp().contains('d')) {
			character.treuremoviment(Input.DRETA);
		} else if(w3.getKeysUp().contains('a')) {
			character.treuremoviment(Input.ESQUERRA);
		}
		
		if(w3.getKeysDown().contains(' ')) {
			character.disparar();
		}

	}

	
}
