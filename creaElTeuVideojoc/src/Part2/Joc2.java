package Part2;

import java.util.ArrayList;


import java.util.*;
import Core.*;

public class Joc2 {
	static Field f2 = new Field();
	static Window w2 = new Window(f2);

	public static void main(String[] args) throws InterruptedException {

		
		ArrayList<Sprite> Sprites = new ArrayList<Sprite>();

		RocaControlable rocaC1 = new RocaControlable(f2, 200, 300, 250,500);

		Sprites.add(rocaC1);

		boolean sortir = false;
		while (!sortir) {
			input(rocaC1);
			f2.draw(Sprites);
			Thread.sleep(30);
		}

	}
	
	static public void input(RocaControlable roquita) {
		
		if(w2.getPressedKeys().contains('w')) {
			roquita.moviment(Input.AMUNT);
		}else if(w2.getPressedKeys().contains('s')) {
			roquita.moviment(Input.AVALL);
		}if(w2.getPressedKeys().contains('d')) {
			roquita.moviment(Input.DRETA);
		}else if(w2.getPressedKeys().contains('a')) {
			roquita.moviment(Input.ESQUERRA);
		}
		// w2.getPressedKeys()
		// w2.getKeysDown()
		// w2.getKeysUp()
	}

}
