package VideoGame;

import Core.*;

public class BloqueRandom extends PhysicBody implements Bloques{
	public BloqueRandom(int x1, int y1, int x2, int y2,Field f) {
    	super("BloqueRandom",x1, y1, x2, y2,0,"resources/blocks/blockRANDOM.gif",f);
    }
	@Override
	public void Golpear(boolean mini) {
		if(!mini) {
			Testing.arraytodo.add(new Flower(this.x1, this.y1-75, this.x2, this.y2-75, this.f));
			
		}else {
			Testing.arraytodo.add(new Champ(this.x1, this.y1-75, this.x2, this.y2-75, this.f));
			
		}
		Testing.arraytodo.add(new BloqueHard(this.x1, this.y1, this.x2, this.y2, this.f));
		this.delete();
	}
	@Override
	public void onCollisionEnter(Sprite sprite) {
		
		
	}
	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

}
