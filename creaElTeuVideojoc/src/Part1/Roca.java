package Part1;

import Core.*;


public class Roca extends Sprite {
	public static int comptador=0;
	public int idroca;
	
	//la variable no sera estatica, ja que no �s una classe amb main
    private int accionsDisponibles; 
    public int getAccionsDisponibles() {
		return accionsDisponibles;
	}

	public void setAccionsDisponibles(int accionsDisponibles) {
		this.accionsDisponibles = accionsDisponibles;
	}


    //constructor generat autom�ticament
    public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
        super(name, x1, y1, x2, y2, angle, path, f);   
        comptador++;
        this.idroca=comptador;
        this.name=this.name+this.idroca;
    }
    
  
    public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int accions) {
        super(name, x1, y1, x2, y2, angle, path, f);   
        this.accionsDisponibles = accions;
        comptador++;
        this.idroca=comptador;
        this.name=this.name+this.idroca;
    }
    

	/**
     * Un constructor que nom�s els hi passes les 4 variables de localitzaci�, el field i l�int d�acci�ns disponibles i assumeix que la roca es dir� roca i tindr� l�imatge de la roca que hem fet servir al punt anterior.
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param angle
     * @param path
     * @param f
     */
    public Roca(int x1, int y1, int x2, int y2,double angle,String path,Field f) {
    	super("Roca",x1, y1, x2, y2,angle,"resources/rock1.png",f);
    	comptador++;
        this.idroca=comptador;
        this.name=this.name+this.idroca;
    }

    /**
     * Un constructor al que li passes el field i 3 ints, x1, y1, i tamany, i crea una roca cuadrada amb aquest tamany de costat, amb 50 accions disponibles
     * @param f
     * @param x1
     * @param y1
     * @param tamany
     */
	public Roca(Field f,int x1,int y1,int tamany) {
		super("Roca", x1, y1, (x1+tamany), (y1+tamany), 0, "resources/rock1.png", f);
		this.accionsDisponibles=50;
		comptador++;
        this.idroca=comptador;
        this.name=this.name+this.idroca;
	}
	
	public Roca() {
		super("Roca",0, 0, 50, 50,0,"resources/rock1.png",Joc1.f);
		this.accionsDisponibles=50;
		comptador++;
        this.idroca=comptador;
       this.name=this.name+this.idroca;
	}
	


}
