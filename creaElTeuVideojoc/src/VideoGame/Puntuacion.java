package VideoGame;

import Core.Field;
import Core.Sprite;

public class Puntuacion extends Sprite{
	private int punts;
	public Puntuacion(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.solid=false;
		this.text=true;
		this.path="SCORE: "+punts;
		this.textColor=0xffffff;
		this.punts=0;
		// TODO Auto-generated constructor stub
	}
	
	public void sumarPuntos(int cuanto) {
		this.punts+=cuanto;
		this.path="SCORE: "+(this.punts);
	}

	public void setPunts(int punts) {
		this.punts = punts;
	}

	public int getPunts() {
		return punts;
	}
	
	
}
