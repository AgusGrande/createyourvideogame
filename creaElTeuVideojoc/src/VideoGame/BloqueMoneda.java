package VideoGame;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class BloqueMoneda extends PhysicBody implements Bloques{
	public BloqueMoneda(int x1, int y1, int x2, int y2,Field f) {
    	super("BloqueRandom",x1, y1, x2, y2,0,"resources/blocks/blockRANDOM.gif",f);
    }
	@Override
	public void Golpear(boolean mini) {
		// TODO Auto-generated method stub
		Testing.puntuacio.sumarPuntos(100);
		Testing.arraytodo.add(new Puntos(x1, y1, x2, y2, f));
		
		this.f.w.playSFX("resources/sounds/coinSound.wav");
		Testing.arraytodo.add(new Coin(this.x1+11, this.y1-75, this.x2-11, this.y2-75, this.f));
		
		Testing.arraytodo.add(new BloqueHard(this.x1, this.y1, this.x2, this.y2, this.f));
		
		
		this.delete();
		
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

}
