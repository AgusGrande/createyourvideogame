package VideoGame;

import java.io.*;
import java.util.ArrayList;

import Core.*;

public class Testing {
	static Field f3 = new Field();
	static Window w3 = new Window(f3);
	static Puntuacion puntuacio = new Puntuacion("nom", 100, 25, 1000, 100, 0, "PROVA", f3);
	static ArrayList<Sprite> arraytodo= new ArrayList<>();

	public static void main(String[] args) throws InterruptedException {

		f3.background = "resources/fondoM3.png";
		Sprite titulo = new Sprite(" ", -800, 500, 0, 800, 0, "resources/title.png", f3);
		arraytodo.add(titulo);
		Sprite BLOQUEO = new Sprite("BLOQUEO", -1, 0, 0, 1000, 0,"", f3);
		arraytodo.add(BLOQUEO);

		for (int i = 1; i <= 8; i++) {
			arraytodo.add(new Suelo("Suelo", (600 * i - 600), 900, (600 * i), 1350, 0, f3));
		}
//		Suelo sueloinimid1 = new Suelo("Suelo", 0, 900, 600, 1350, 0, f3);
//		Suelo sueloinimid2 = new Suelo("Suelo", 600, 900, 1200, 1350, 0, f3);
//		Suelo sueloinimid3 = new Suelo("Suelo", 1200, 900, 1800, 1350, 0, f3);
//		Suelo sueloinimid4 = new Suelo("Suelo", 1800, 900, 2400, 1350, 0, f3);
//		Suelo sueloinimid5 = new Suelo("Suelo", 2400, 900, 3000, 1350, 0, f3);
//		Suelo sueloinimid6 = new Suelo("Suelo", 3000, 900, 3600, 1350, 0, f3);
//		Suelo sueloinimid7 = new Suelo("Suelo", 3600, 900, 4200, 1350, 0, f3);
//		Suelo sueloinimid8 = new Suelo("Suelo", 4200, 900, 4800, 1350, 0, f3);

		BloqueMoneda blockCoin1 = new BloqueMoneda(475, 500, 550, 575, f3);
		arraytodo.add(blockCoin1);
		BloqueLadrillo block111 = new BloqueLadrillo(975, 500, 1050, 575, f3);
		arraytodo.add(block111);
		BloqueRandom blockCoin2 = new BloqueRandom(1050, 500, 1125, 575, f3);
		arraytodo.add(blockCoin2);
		BloqueLadrillo block113 = new BloqueLadrillo(1125, 500, 1200, 575, f3);
		arraytodo.add(block113);
		BloqueMoneda blockCoin3 = new BloqueMoneda(1200, 500, 1275, 575, f3);
		arraytodo.add(blockCoin3);
		BloqueLadrillo block115 = new BloqueLadrillo(1275, 500, 1350, 575, f3);
		arraytodo.add(block115);
		BloqueMoneda block0 = new BloqueMoneda(1125, 200, 1200, 275, f3);
		arraytodo.add(block0);

		Tuberias pipe1 = new Tuberias(2000, 750, 2225, 900, "resources/misc/pipeS.png", f3);
		arraytodo.add(pipe1);
		Tuberias pipe2 = new Tuberias(2900, 675, 3125, 900, "resources/misc/pipeM.png", f3);
		arraytodo.add(pipe2);
		Tuberias pipe3 = new Tuberias(3800, 600, 4025, 900, "resources/misc/pipeL.png", f3);
		arraytodo.add(pipe3);

		Goomba Enemigo1 = new Goomba(f3, 1500, 825, 75);
		arraytodo.add(Enemigo1);
		Koopa Enemigo2 = new Koopa(f3, 2725, 788, 2800, 900);
		arraytodo.add(Enemigo2);
		Goomba Enemigo3 = new Goomba(f3, 3500, 825, 75);
		arraytodo.add(Enemigo3);
		Goomba Enemigo4 = new Goomba(f3, 3700, 825, 75);
		arraytodo.add(Enemigo4);

		Suelo suelomid1 = new Suelo("Suelo", 5200, 900, 5800, 1350, 0, f3);
		arraytodo.add(suelomid1);
		Suelo suelomid2 = new Suelo("Suelo", 5800, 900, 6400, 1350, 0, f3);
		arraytodo.add(suelomid2);
		BloqueLadrillo blockL1 = new BloqueLadrillo(5650, 500, 5725, 575, f3);
		arraytodo.add(blockL1);
		BloqueRandom blockCoinL1 = new BloqueRandom(5725, 500, 5800, 575, f3);
		arraytodo.add(blockCoinL1);
		BloqueLadrillo blockL2 = new BloqueLadrillo(5800, 500, 5875, 575, f3);
		arraytodo.add(blockL2);
		
		BloqueLadrillo blockF = new BloqueLadrillo(5875, 200, 5950, 275, f3);
		arraytodo.add(blockF);
		BloqueLadrillo blockF1 = new BloqueLadrillo(5950, 200, 6025, 275, f3);
		arraytodo.add(blockF1);
		BloqueLadrillo blockF2 = new BloqueLadrillo(6025, 200, 6100, 275, f3);
		arraytodo.add(blockF2);
		BloqueLadrillo blockF3 = new BloqueLadrillo(6100, 200, 6175, 275, f3);
		arraytodo.add(blockF3);
		BloqueLadrillo blockF4 = new BloqueLadrillo(6175, 200, 6250, 275, f3);
		arraytodo.add(blockF4);
		BloqueLadrillo blockF5 = new BloqueLadrillo(6250, 200, 6325, 275, f3);
		arraytodo.add(blockF5);
		BloqueLadrillo blockF6 = new BloqueLadrillo(6325, 200, 6400, 275, f3);
		arraytodo.add(blockF6);
		BloqueLadrillo blockF7 = new BloqueLadrillo(6400, 200, 6475, 275, f3);
		arraytodo.add(blockF7);
		BloqueLadrillo blockF8 = new BloqueLadrillo(6475, 200, 6550, 275, f3);
		arraytodo.add(blockF8);
		BloqueLadrillo blockF9 = new BloqueLadrillo(6550, 200, 6625, 275, f3);
		arraytodo.add(blockF9);
		Goomba Enemigo5 = new Goomba(f3, 6550, 125, 75);
		arraytodo.add(Enemigo5);

		BloqueLadrillo blockFF2 = new BloqueLadrillo(6925, 200, 7000, 275, f3);
		arraytodo.add(blockFF2);
		BloqueLadrillo blockFF3 = new BloqueLadrillo(7000, 200, 7075, 275, f3);
		arraytodo.add(blockFF3);
		BloqueLadrillo blockFF4 = new BloqueLadrillo(7075, 200, 7150, 275, f3);
		arraytodo.add(blockFF4);
		BloqueMoneda blockFF5 = new BloqueMoneda(7150, 200, 7225, 275, f3);
		arraytodo.add(blockFF5);

		BloqueLadrillo blockFF6 = new BloqueLadrillo(7150, 500, 7225, 575, f3);
		arraytodo.add(blockFF6);

		BloqueLadrillo blockFF7 = new BloqueLadrillo(7625, 500, 7700, 575, f3);
		arraytodo.add(blockFF7);
		BloqueLadrillo blockFF8 = new BloqueLadrillo(7700, 500, 7775, 575, f3);
		arraytodo.add(blockFF8);

		Suelo suelomid3 = new Suelo("Suelo", 6800, 900, 7400, 1350, 0, f3);
		arraytodo.add(suelomid3);
		Suelo suelomid4 = new Suelo("Suelo", 7400, 900, 8000, 1350, 0, f3);
		arraytodo.add(suelomid4);
		Suelo suelomid5 = new Suelo("Suelo", 8000, 900, 8600, 1350, 0, f3);
		arraytodo.add(suelomid5);
		Suelo suelomid6 = new Suelo("Suelo", 8600, 900, 9200, 1350, 0, f3);
		arraytodo.add(suelomid6);
		Suelo suelomid7 = new Suelo("Suelo", 9200, 900, 9800, 1350, 0, f3);
		arraytodo.add(suelomid7);

		BloqueMoneda blockFAFlying = new BloqueMoneda(8400, 200, 8475, 275, f3);
		arraytodo.add(blockFAFlying);
		BloqueMoneda blockFA1 = new BloqueMoneda(8175, 500, 8250, 575, f3);
		arraytodo.add(blockFA1);
		BloqueMoneda blockFA2 = new BloqueMoneda(8400, 500, 8475, 575, f3);
		arraytodo.add(blockFA2);
		BloqueMoneda blockFA3 = new BloqueMoneda(8625, 500, 8700, 575, f3);
		arraytodo.add(blockFA3);

		castle castillo = new castle(9200, 0, 9800, 900, f3);
		arraytodo.add(castillo);

		Flower flor = new Flower(2075, 675, 2150, 750, f3);
		arraytodo.add(flor);

		FireBall projectil1 = new FireBall("FireBall", 0, 0, 3, 3, "resources/misc/fire.gif", f3);
		arraytodo.add(projectil1);
		Personatge elPersonaje = new Personatge(f3, 0, 825, 65, 900, "resources/mario-mini/mario-mini.png", projectil1);
		arraytodo.add(elPersonaje);

		w3.playMusic("resources/sounds/songMario.wav");
		w3.musicVolume = 100;
		boolean sortir = false;
		while (!sortir) {
			input(elPersonaje);
			f3.draw(arraytodo);
			//f3.draw();
			Thread.sleep(10);
			f3.lockScroll(elPersonaje, w3, "EsteEsMiMetodo");
			
		}

	}

	static public void input(Personatge character) {
		if (w3.getKeysDown().contains('w')) {
			character.moviment(Input.AMUNT);
		}
		if (w3.getPressedKeys().contains('d')) {
			character.moviment(Input.DRETA);
		} else if (w3.getPressedKeys().contains('a')) {
			character.moviment(Input.ESQUERRA);
		}

		if (w3.getKeysUp().contains('d')) {
			character.treuremoviment(Input.DRETA);
		} else if (w3.getKeysUp().contains('a')) {
			character.treuremoviment(Input.ESQUERRA);
		}

		if (w3.getKeysDown().contains(' ')) {
			character.disparar();
		}
		if (character.y1 > 4200) {
			character.y1 = 4200;
			character.morir();
		}
		
		
		/////////GUARDADO PARTIDA
		if (w3.getKeysDown().contains('1')) {
			try {
				File f = new File("character.dat");
				FileOutputStream fos = new FileOutputStream(f, false);
				AppendableObjectOutputStream oos = new AppendableObjectOutputStream(fos, true);
				oos.writeObject(arraytodo);
				oos.flush();
				oos.close();
				System.out.println("GUARDADO CORRECTAMENTE");

			} catch (FileNotFoundException e) {
				System.out.println("no existeix el fitxer");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("excepci� d'entrada/sortida");
				e.printStackTrace();
			}
		}
		
		
		if (w3.getKeysDown().contains('2')) {
			try {
				
				File f = new File("character.dat");
				FileInputStream fis = new FileInputStream(f);
				ObjectInputStream ois = new ObjectInputStream(fis);
				
				
				try {
					for(;;) {
						ArrayList<Sprite> leida = (ArrayList<Sprite>) ois.readObject();
						for(int i=0;i<leida.size();i++) {
							if (leida.get(i) instanceof Personatge) {
								Personatge llegit=(Personatge) leida.get(i);
								character.x1=llegit.x1;
								character.x2=llegit.x2;
								character.y1=llegit.y1;
								character.y2=llegit.y2;
								
								character.mini=llegit.mini;
								character.fire=llegit.fire;
								character.path=llegit.path;
							} else if(leida.get(i) instanceof Puntuacion) {
								Puntuacion puntuacioT= (Puntuacion) leida.get(i);
								puntuacio.setPunts(puntuacioT.getPunts());
								puntuacio.path=puntuacioT.path;
							}
							/*
							else {

							arraytodo.get(i).x1=leida.get(i).x1;
							arraytodo.get(i).x2=leida.get(i).x2;
							arraytodo.get(i).y1=leida.get(i).y1;
							arraytodo.get(i).y2=leida.get(i).y2;
							}*/
						}
						
					}
				} catch (EOFException ee) {
					
				}
				
				ois.close();
				

			} catch (FileNotFoundException e) {
				System.out.println("no existeix el fitxer");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("excepci� d'entrada/sortida");
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		
		

	}



}
