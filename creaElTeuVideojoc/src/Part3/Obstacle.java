package Part3;

import Core.*;

public class Obstacle extends PhysicBody implements Disparable{
	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
	
	public Obstacle(Field f,int x1,int y1,int tamany,String path) {
		super("TheObstacle", x1, y1, (x1+tamany), (y1+tamany), 0, path, f);
	}
	public Obstacle(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
        super(name, x1, y1, x2, y2, angle, path, f); 
    }
	
	public void danyar() {
		this.delete();
	}

}
