package Part3;

import Core.*;
public class Projectil extends PhysicBody {
	int LAUNCHposition;
	
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
	}

	public Projectil(String nom, int x1, int y1, int x2, int y2,String path,Field f) {
		super(nom, x1, y1, x2,y2,0,path,f);
		this.trigger=true;
		this.addVelocity(10, 0);
	}
	
	public Projectil(Projectil p, int x1, int y1, int x2, int y2,Field f) {
		this(p.name,x1,y1,x2,y2,p.path, f);
		this.LAUNCHposition=this.x1;
	}
	
	
	public void onCollisionEnter(Sprite spriteTocado) {
        System.out.println("Colisionado con "+spriteTocado.name);
        if(spriteTocado instanceof Disparable) {
            Disparable elobjetivo = (Disparable) spriteTocado; // Casteo a disparable
            elobjetivo.danyar(); 
            System.out.println("PUMMM!");
            this.delete(); //se borra el proyectil al haber impactado con un objetivo disparable
        }
    }
	public void update() {
		if(this.x1>(this.LAUNCHposition+1500)) {
			this.delete();
		}
	}
	

	
}
