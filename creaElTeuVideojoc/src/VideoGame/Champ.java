package VideoGame;

import Core.*;

public class Champ extends PowerUps {

	public Champ(int x1, int y1, int x2, int y2, Field f) {
		super("champ", x1, y1, x2, y2, "resources/misc/champ.png", f, PowerUpTipus.CHAMP);
		this.velocity[0] = 3.5;
	}

	public void onCollisionEnter(Sprite spriteTocado) {
		if (spriteTocado.y1 < this.y2) {
			if (x2<=spriteTocado.x1) {
				this.velocity[0] = -3.5;
			} else {
				this.velocity[0] = 3.5;
			}
		}

	}

	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	

}
