package VideoGame;

import Core.*;

public class BloqueRoto extends Sprite{
	int cicles;

	public BloqueRoto( int x1, int y1, int x2, int y2,  Field f) {
		super("BloqueRoto", x1, y1, x2, y2, 0, "resources/blocks/blockBroken.png", f);
		this.cicles=4;
		this.solid=false;//este parametro para evitar que colisione y obstruya al Champ por ejemplo
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		super.update();
		this.cicles--;
		if(this.cicles==0) {
			this.delete();
		}
	}

}
