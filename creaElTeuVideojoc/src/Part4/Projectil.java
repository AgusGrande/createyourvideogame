package Part4;

import Core.*;
public class Projectil extends PhysicBody {
	
	public void onCollisionExit(Sprite sprite) {
		
	}

	public Projectil(String nom, int x1, int y1, int x2, int y2,String path,Field f) {
		super(nom, x1, y1, x2,y2,0,path,f);
		this.trigger=true;
		this.addVelocity(0, -10);
	}
	
	public Projectil(Projectil p, int x1, int y1, int x2, int y2,Field f) {
		this(p.name,x1+45,y1-40,x2-45,y1,p.path, f);
	}
	
	
	public void onCollisionEnter(Sprite spriteTocado) {
        if(spriteTocado instanceof Disparable) {
            Disparable elobjetivo = (Disparable) spriteTocado; // Casteo a disparable
            elobjetivo.danyar(); 
            this.f.w.playSFX("resources/ASI_impactoDado.wav");
            Puntuacio.punts+=1;
            this.delete(); //se borra el proyectil al haber impactado con un objetivo disparable
        }
    }
	public void update() {
		if(this.y1<0) {
			this.delete();
		}
	}
	

	
}
