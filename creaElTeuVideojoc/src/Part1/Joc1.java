package Part1;

import java.util.*;
import Core.*;

public class Joc1 {
	//Declaracion de variables estaticas field y window
	static Field f = new Field();
	static Window w = new Window(f);
	
	public static void main(String[] args) throws InterruptedException {
		
		ArrayList<Sprite> Sprites=new ArrayList<Sprite>();
		
		Sprite roca = new Sprite("Roca", 50, 50, 100, 150, 0, "resources/rock1.png", f );
		Sprites.add(roca);
		
		Roca r1 = new Roca("roca", 50, 50, 100, 100, 0, "resources/rock1.png", f);
        Roca r2 = new Roca("roca", 50, 50, 100, 100, 0, "resources/rock1.png", f);
        
        System.out.println(roca.name);
        System.out.println(r1.name);
        System.out.println(r2.name);
		
		
		 boolean sortir = false;
	        while (!sortir) {
	            f.draw(Sprites);
	            Thread.sleep(30);
	        }
	        

		
	}

}
