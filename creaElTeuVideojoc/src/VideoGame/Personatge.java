package VideoGame;

import Core.*;

public class Personatge extends PhysicBody {

	private FireBall projectil1;
	private int salt = 1;
	protected boolean mini;
	protected boolean fire;

	public Personatge(Field f, int x1, int y1, int x2, int y2, String path, FireBall proj) {
		super("TheCharacter", x1, y1, x2, y2, 0, path, f);
		this.addConstantForce(0, 0.6);
		this.projectil1 = proj;
		this.mini = true;
		this.fire = false;
	}

	public void disparar() {
		if (this.fire) {
			if (!this.flippedX) {
				Testing.arraytodo.add(new FireBall(this.projectil1, this.x2, this.y2 - 100, this.x2 + 50, this.y2 - 50, this.f, "R"));
				
			} else {
				Testing.arraytodo.add(new FireBall(this.projectil1, this.x1 - 50, this.y2 - 100, this.x1, this.y2 - 50, this.f, "L"));
				
			}
			this.f.w.playSFX("resources/sounds/marioFireBall.wav");
		}

	}

	public void onCollisionEnter(Sprite spriteTocado) {
		if ((spriteTocado instanceof Suelo || spriteTocado instanceof Bloques || spriteTocado instanceof Tuberias)
				&& (this.y2 <= spriteTocado.y1)) {
			if (this.salt < 1) {
				if (this.mini) {
					this.path = "resources/mario-mini/mario-mini.png";
				} else {
					if (!this.fire) {
						this.path = "resources/mario-normal/mario-normal.png";
					} else {
						this.path = "resources/mario-fire/mario-fire.png";
					}

				}

			}
			this.salt = 1;
		} else if (spriteTocado instanceof Enemigos) {
			if (spriteTocado instanceof Goomba) {
				Goomba elGoomba = (Goomba) spriteTocado;
				if (this.y2 <= elGoomba.y1) {
					elGoomba.danyar();
					this.f.w.playSFX("resources/sounds/goombaStomp.wav");
					this.setForce(0, -3.33);
				} else {
					this.morir();
				}
			}
			if (spriteTocado instanceof Koopa) {
				Koopa elKoopa = (Koopa) spriteTocado;
				if (elKoopa.isEscondido() && !elKoopa.isMovimiento()) {
					elKoopa.danyar();
					this.f.w.playSFX("resources/sounds/goombaStomp.wav");
				} else if (this.y2 <= elKoopa.y1 + 17) {
					elKoopa.danyar();
					Testing.puntuacio.sumarPuntos(100);
					this.f.w.playSFX("resources/sounds/goombaStomp.wav");
					this.setForce(0, -3.33);
				} else {
					this.morir();
				}
			}
		}

		if (spriteTocado instanceof Bloques && spriteTocado.y2 <= this.y1) {
			Bloques bloquetocado = (Bloques) spriteTocado;
			bloquetocado.Golpear(this.mini);
		}
		if (spriteTocado instanceof PowerUps) {
			PowerUps powerTocado = (PowerUps) spriteTocado;
			powerTocado.cogido();
			if (powerTocado.getTipo() == PowerUpTipus.CHAMP) {
				this.Crecer();
			} else if (powerTocado.getTipo() == PowerUpTipus.FLOWER) {
				this.fire = true;
				this.Crecer();
			}
			Testing.puntuacio.sumarPuntos(100);
			Testing.arraytodo.add(new Puntos(powerTocado.x1, powerTocado.y1, powerTocado.x2, powerTocado.y2, f));
			

		}
		if (spriteTocado instanceof castle) {
			System.out.println("HAS GUANYAT");
			System.exit(0);
		}

	}

	public void onCollisionExit(Sprite sprite) {

	}

	public void Crecer() {
		if (this.mini) {
			this.y1 -= 75;
			this.mini = false;
			this.f.w.playSFX("resources/sounds/marioBig.wav");
			this.path = "resources/mario-normal/mario-normal.png";
		}
		if (this.fire) {
			if (this.mini == false) {
				this.f.w.playSFX("resources/sounds/marioBig.wav");
			}
			this.path = "resources/mario-fire/mario-fire.png";
		}
	}

	public void moviment(Input in) {
		if (in == Input.AMUNT && this.salt > 0) {
			this.salt--;
			this.setForce(0, -5.6);
			if (this.mini) {
				this.path = "resources/mario-mini/mario-miniJ.png";
			} else {
				if (!this.fire) {
					this.path = "resources/mario-normal/mario-normalJ.png";
				} else {
					this.path = "resources/mario-fire/mario-fireJ.png";
				}

			}

			this.f.w.playSFX("resources/sounds/marioJump.wav");
		} else if (in == Input.DRETA) {
			this.velocity[0] = 8;
			this.flippedX = false;
			if (this.salt > 0) {// si aun tengo saltos(no he saltado) cambia animacion
				if (this.mini) {
					this.path = "resources/mario-mini/mario-mini.gif";
				} else {
					if (!this.fire) {
						this.path = "resources/mario-normal/mario-normal.gif";
					} else {
						this.path = "resources/mario-fire/mario-fire.gif";
					}

				}

			}
		} else if (in == Input.ESQUERRA) {
			this.velocity[0] = -8;
			this.flippedX = true;
			if (this.salt > 0) {// si aun tengo saltos(no he saltado) cambia animacion
				if (this.mini) {
					this.path = "resources/mario-mini/mario-mini.gif";
				} else {
					if (!this.fire) {
						this.path = "resources/mario-normal/mario-normal.gif";
					} else {
						this.path = "resources/mario-fire/mario-fire.gif";
					}

				}

			}
		}
	}

	public void treuremoviment(Input in) {
		if (in == Input.DRETA) {
			if (this.velocity[0] == 8) {
				this.addVelocity(-8, 0);
			}
			if (this.salt > 0) {// si aun tengo saltos(no he saltado) cambia animacion
				if (this.mini) {
					this.path = "resources/mario-mini/mario-mini.png";
				} else {
					if (!this.fire) {
						this.path = "resources/mario-normal/mario-normal.png";
					} else {
						this.path = "resources/mario-fire/mario-fire.png";
					}
				}

			}
		} else if (in == Input.ESQUERRA) {
			if (this.velocity[0] == -8) {
				this.addVelocity(8, 0);
			}
			if (this.salt > 0) {// si aun tengo saltos(no he saltado) cambia animacion
				if (this.mini) {
					this.path = "resources/mario-mini/mario-mini.png";
				} else {
					if (!this.fire) {
						this.path = "resources/mario-normal/mario-normal.png";
					} else {
						this.path = "resources/mario-fire/mario-fire.png";
					}

				}
			}

		}
	}

	public void morir() {
		if (this.mini) {
			this.delete();
			this.f.w.playMusic("");
			this.f.w.playSFX("resources/sounds/marioDead.wav");
			System.out.println("HAS PERDUT!!!!");

		} else {
			if (!this.fire) {
				this.f.w.playSFX("resources/sounds/marioShrink.wav");
				this.mini = true;
				this.y1 += 75;
				this.path="resources/mario-mini/mario-mini.png";
			} else {
				this.f.w.playSFX("resources/sounds/marioShrink.wav");
				this.path="resources/mario-normal/mario-normal.png";
				this.fire = false;
			}

		}
	}

}
