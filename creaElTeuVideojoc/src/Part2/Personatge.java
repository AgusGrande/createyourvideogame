package Part2;

import Core.*;
import Part1.*;

public class Personatge extends PhysicBody {
	int salts=2;
	boolean aterra=false;
	public Personatge(Field f,int x1,int y1,int tamany) {
		super("Roca", x1, y1, (x1+tamany), (y1+tamany), 0, "resources/terraria.gif", f);
		this.addConstantForce(0, 0.2);
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		if(sprite instanceof Roca) {
			aterra=true;
			this.salts=2;
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		if(sprite instanceof Roca) {
			aterra=false;
		}
	}
	
	public void moviment(Input in) {
			if(in == Input.AMUNT && (aterra || salts>0)){
				this.setForce(0, -2);
				this.salts--;
				//this.addForce(0, -1);
			}else if(in==Input.DRETA) {
				this.addVelocity(8, 0);
				//this.setVewwwlocity(10, 0);
			}else if(in==Input.ESQUERRA) {
				this.addVelocity(-8, 0);
				//this.setVelocity(-10, 0);
			}
	}
	public void treuremoviment(Input in) {
		if(in==Input.DRETA) {
			this.addVelocity(-8, 0);
		}else if(in==Input.ESQUERRA) {
			this.addVelocity(8, 0);
		}
	}

}
