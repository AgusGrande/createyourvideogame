package Part2;

import java.util.ArrayList;


import java.util.*;
import Core.*;
import Part1.*;

public class TestJoc {
	static Field f2 = new Field();
	static Window w2 = new Window(f2);

	public static void main(String[] args) throws InterruptedException {

		
		ArrayList<Sprite> Sprites = new ArrayList<Sprite>();


		Roca roca = new Roca("Roca", 0, 800, 900, 1250, 0, "resources/suelo.jpg", f2 );
		Roca roca1 = new Roca("Roca", 1100, 800, 2000, 1300, 0, "resources/suelo.jpg", f2 );
		Sprites.add(roca);
		Sprites.add(roca1);
		
		Personatge mega = new Personatge(f2, 200, 300, 150);
		Sprites.add(mega);

		boolean sortir = false;
		while (!sortir) {
			input(mega);
			f2.draw(Sprites);
			Thread.sleep(15);
		}

	}
	
	static public void input(Personatge character) {
		
		if(w2.getKeysDown().contains('w')) {
			character.moviment(Input.AMUNT);
		}else if(w2.getKeysDown().contains('d')) {
			character.moviment(Input.DRETA);
		}else if(w2.getKeysDown().contains('a')) {
			character.moviment(Input.ESQUERRA);
		}
		if(w2.getKeysUp().contains('d')) {
			character.treuremoviment(Input.DRETA);
		}else if(w2.getKeysUp().contains('a')) {
			character.treuremoviment(Input.ESQUERRA);
		}
		// w2.getPressedKeys()
		// w2.getKeysDown()
		// w2.getKeysUp()
	}

}
