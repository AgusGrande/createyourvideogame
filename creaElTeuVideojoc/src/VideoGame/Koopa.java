package VideoGame;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Koopa extends PhysicBody implements Enemigos {
	public boolean escondido;
	public boolean isEscondido() {
		return escondido;
	}
	public boolean isMovimiento() {
		return movimiento;
	}
	private boolean movimiento;
	private int cicles;
	public Koopa(Field f, int x1, int y1, int x2,int y2) {
		super("TheEnemy", x1, y1, x2, y2 , 0, "resources/enemies/koopa.gif", f);
		this.velocity[0] = -2;
		this.escondido=false;
		this.addConstantForce(0, 0.6);
		this.movimiento=false;
	}
	private void salirDeNuevo() {
		this.escondido=false;
		this.movimiento=false;
		this.y1-=37;
		this.path="resources/enemies/koopa.gif";
		this.velocity[0] = -2;
		this.flippedX=false;
	}
	public void danyar() {
		Testing.puntuacio.sumarPuntos(100);
		if(!this.escondido) {
			this.escondido=true;
			this.cicles=250;
			this.velocity[0]=0;
			this.y1+=37;
			this.path = "resources/enemies/koopaH.png";
		}else {
			this.movimiento=true;
		}
		Testing.arraytodo.add(new Puntos(x1, y1, x2, y2, f));
		
		
	}
	@Override
	public void onCollisionEnter(Sprite spriteTocado) {
		if (spriteTocado.y1 < this.y2) {
			if(escondido) {
				if (x1>=spriteTocado.x2) {
					this.velocity[0] = 6;
					this.flippedX=false;
				} else {
					this.velocity[0] = -6;
					this.flippedX=true;
				}
			}else {
				if (x2<=spriteTocado.x1) {
					this.velocity[0] = -2;
					this.flippedX=false;
				} else {
					this.velocity[0] = 2;
					this.flippedX=true;
				}
			}
		}
	}
	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		super.update();
		if(escondido && !movimiento) {
			this.cicles--;
			if(cicles==0) {
				this.salirDeNuevo();
			}
		}
		
	}
}
