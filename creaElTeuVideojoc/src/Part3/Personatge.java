package Part3;

import Core.*;


public class Personatge extends PhysicBody {
	int salts=2;
	Projectil projectil1;
	direccion MIRANDO;
	
	public Personatge(Field f,int x1,int y1,int tamany,String path,Projectil proj) {
		super("TheCharacter", x1, y1, (x1+tamany), (y1+tamany), 0, path, f);
		this.addConstantForce(0, 0.2);
		this.projectil1=proj;
		MIRANDO=direccion.DERECHA;
	}
	
	public void disparar() {
		new Projectil(this.projectil1, this.x1, this.y1, this.x2, this.y2,this.f);
	}

	public void onCollisionEnter(Sprite spriteTocado) {
		if(spriteTocado instanceof Roca) {
			this.salts=2;
		}
		else if(spriteTocado instanceof Pinchos) {
        	this.morir();
        }else if(spriteTocado instanceof Enemic) {
        	Enemic elEnemigo = (Enemic) spriteTocado; 
        	if(this.y2 <= elEnemigo.y1) {
        		elEnemigo.danyar();
        		this.setForce(0, -2);
        	}else {
        		this.morir();
        	} 
        }
	}

	public void onCollisionExit(Sprite sprite) {
	}

	
	public void moviment(Input in) {
			if(in == Input.AMUNT && (salts>0)){
				this.setForce(0, -2);
				this.salts--;
			} else if(in==Input.DRETA) {
				this.velocity[0]=8;
			} else if(in==Input.ESQUERRA) {
				this.velocity[0]=-8;
			}
	}
	public void treuremoviment(Input in) {
		if(in==Input.DRETA) {
			if(this.velocity[0]==8) {
				this.addVelocity(-8, 0);
			}
		} else if(in==Input.ESQUERRA) {
			if(this.velocity[0]==-8) {
				this.addVelocity(8, 0);
			}
			
		}
	}
	public void morir() {
		this.delete();
	}
	
	


}
