package Part4;

import java.util.*;
import Core.*;

public class Spawner {
	static Field field;
	static int xMax;

	static Timer timer = new Timer();
	
	static TimerTask task1 = new TimerTask() {
		public void run() {
			generarEnemic();
		}
	};

	public Spawner(Field f, int Xmax) {
		this.field = f;
		this.xMax = Xmax;
		timer.schedule(task1, 0, 1000);
	}

	static public void generarEnemic() {
		Random ran = new Random();
		int xRandom = ran.nextInt(xMax);
		if (xRandom < 0) {
			xRandom = 0;
		}
		new EnemicBasic(field, xRandom, 25, xRandom + 100, 100, "resources/InvaderB1.png", 1);
	}
}
