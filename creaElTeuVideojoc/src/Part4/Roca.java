package Part4;

import Core.*;


public class Roca extends Sprite {
	public static int comptador=0;
	public int idroca;
	


    //constructor generat autom�ticament
    public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
        super(name, x1, y1, x2, y2, angle, path, f);   
        comptador++;
        this.idroca=comptador;
        this.name=this.name+this.idroca;
    }
  

	/**
     * Un constructor que nom�s els hi passes les 4 variables de localitzaci�, el field i l�int d�acci�ns disponibles i assumeix que la roca es dir� roca i tindr� l�imatge de la roca que hem fet servir al punt anterior.
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param angle
     * @param path
     * @param f
     */
    public Roca(int x1, int y1, int x2, int y2,double angle,String path,Field f) {
    	super("Roca",x1, y1, x2, y2,angle,"resources/rock1.png",f);
    	comptador++;
        this.idroca=comptador;
        this.name=this.name+this.idroca;
    }

	
	


}
