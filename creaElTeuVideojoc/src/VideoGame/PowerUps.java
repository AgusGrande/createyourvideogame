package VideoGame;

import Core.Field;
import Core.PhysicBody;

public abstract class PowerUps extends PhysicBody{
	private PowerUpTipus tipo;
	public PowerUps(String nom,int x1, int y1, int x2, int y2,String path, Field f,PowerUpTipus tipo) {
		super(nom, x1, y1, x2, y2, 0, path, f);
		this.addConstantForce(0, 0.81);
		this.setTipo(tipo);
	}
	public void cogido() {
		this.delete();
	}
	public PowerUpTipus getTipo() {
		return tipo;
	}
	public void setTipo(PowerUpTipus tipo) {
		this.tipo = tipo;
	}
}
