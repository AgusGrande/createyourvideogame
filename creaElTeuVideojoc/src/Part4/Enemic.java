package Part4;


import Core.*;

public class Enemic extends PhysicBody implements Disparable{

	int vida;
	
	
	public Enemic(Field f,int x1,int y1,int x2,int y2,String path,int vida) {
		super("TheEnemy", x1, y1, x2, y2, 0, path, f);
		this.vida=vida;
	}

	public void danyar() {
		if(this.vida>1) {
			this.vida--;
		}else {
			this.vida--;
			this.delete();
		}
	}
	
	@Override
	public void onCollisionEnter(Sprite spriteTocado) {
		
		
	}

	public void onCollisionExit(Sprite sprite) {

		
	}
	
	

}
