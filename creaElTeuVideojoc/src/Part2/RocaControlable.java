package Part2;

import Core.*;
import Part1.*;

public class RocaControlable extends Roca {
	
	/**
	 * Crea una roca llamando al super con las mismas variables
	 * al constructor
	 * @param f
	 * @param x1
	 * @param y1
	 * @param tamany
	 */
	public RocaControlable(Field f,int x1,int y1,int tamany,int accions) {
		super(f, x1, y1, tamany);
		this.setAccionsDisponibles(accions);
	}
	
	
	public void moviment(Input in) {
		if(this.getAccionsDisponibles()<=0) {
			this.delete();
		}else {
			this.setAccionsDisponibles(this.getAccionsDisponibles()-1);
			if(in == Input.AMUNT){
			    this.y1-=20;
			    this.y2-=20;
			}else if(in==Input.AVALL) {
				this.y1+=20;
				this.y2+=20;
			}else if(in==Input.DRETA) {
				this.x1+=20;
				this.x2+=20;
			}else if(in==Input.ESQUERRA) {
				this.x1-=20;
				this.x2-=20;
			}
		}
	}
	

}
