package VideoGame;

import Core.*;
public class FireBall extends PhysicBody {
	int ciclos;
	
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
	}

	public FireBall(String nom, int x1, int y1, int x2, int y2,String path,Field f) {
		super(nom, x1, y1, x2,y2,0,path,f);
		this.addConstantForce(0, 1);
		this.physicBody=false;
		this.ciclos=111;
	}
	
	public FireBall(FireBall p, int x1, int y1, int x2, int y2,Field f,String side) {
		this(p.name,x1,y1,x2,y2,p.path, f);
		if(side.equals("R")) {
			this.addVelocity(15, 0);
		}else {
			this.addVelocity(-15, 0);
		}
	}
	
	
	public void onCollisionEnter(Sprite spriteTocado) {
		if(spriteTocado instanceof FireBall) {
			this.delete();
		}
        if(spriteTocado instanceof Enemigos) {
            Enemigos elobjetivo = (Enemigos) spriteTocado; // Casteo a Enemigos
            elobjetivo.danyar(); 
            spriteTocado.delete();
            this.delete(); //se borra el proyectil al haber impactado con un objetivo Enemigos
        }
        if(spriteTocado.y1>=this.y2) {
        	this.setForce(0, -5);
        }
    }
	public void update() {
		this.ciclos--;
		if(this.ciclos<=0) {
			this.delete();
		}
	}
	

	
}
