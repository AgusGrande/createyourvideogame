package Part4;

import Core.*;

public class EnemicBasic extends Enemic{

	public EnemicBasic(Field f, int x1, int y1, int x2, int y2, String path, int vida) {
		super(f, x1, y1, x2, y2, path, vida);
		this.setVelocity(0, 6);
	}
	
	public void onCollisionEnter(Sprite spriteTocado) {
		if(spriteTocado instanceof Nau) {
			Nau elpersonatge = (Nau) spriteTocado; 
			elpersonatge.delete();
			this.delete();
        }
		
	}

	

}
