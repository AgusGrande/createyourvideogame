package VideoGame;

import Core.Field;
import Core.Sprite;

public class Coin extends Sprite{
	int cicles;

	public Coin( int x1, int y1, int x2, int y2,  Field f) {
		super("Coin", x1, y1, x2, y2, 0, "resources/misc/coin.gif", f);
		this.cicles=15;
		this.solid=false;//este parametro para evitar que colisione y obstruya al Champ por ejemplo
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		super.update();
		this.cicles--;
		if(this.cicles==0) {
			this.delete();
		}
	}

	
	

}
