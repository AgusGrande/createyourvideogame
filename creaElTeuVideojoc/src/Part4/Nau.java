package Part4;

import Core.*;


public class Nau extends PhysicBody {
Projectil projectil1;
	
	
	public Nau(Field f,int x1,int y1,int x2,int y2,String path,Projectil proj) {
		super("TheCharacter", x1, y1, x2, y2, 0, path, f);
		this.projectil1=proj;
	}
	
	public Nau(Field f,int x1,int y1,int tamany,String path,Projectil proj) {
		super("TheCharacter", x1, y1, (x1+tamany), (y1+tamany), 0, path, f);
		this.projectil1=proj;
	}
	
	public void disparar() {
		new Projectil(this.projectil1, this.x1, this.y1, this.x2, this.y2,this.f);
		this.f.w.playSFX("resources/ASI_disparo.wav");
	}

	public void onCollisionEnter(Sprite spriteTocado) {
		
	}

	public void onCollisionExit(Sprite sprite) {
	}

	
	public void moviment(Input in) {
			if(in==Input.DRETA) {
				this.velocity[0]=12;
			} else if(in==Input.ESQUERRA) {
				this.velocity[0]=-12;
			}
	}
	public void treuremoviment(Input in) {
		if(in==Input.DRETA) {
			if(this.velocity[0]==12) {
				this.addVelocity(-12, 0);
			}
		} else if(in==Input.ESQUERRA) {
			if(this.velocity[0]==-12) {
				this.addVelocity(12, 0);
			}
			
		}
	}
	


}
