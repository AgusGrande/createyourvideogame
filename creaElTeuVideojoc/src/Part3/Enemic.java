package Part3;


import Core.*;

public class Enemic extends PhysicBody implements Disparable{

	int vida;
	boolean dormido;
	
	/**
	 * Crea un objeto de la classe enemigo
	 * @param f
	 * @param x1
	 * @param y1
	 * @param tamany
	 * @param path
	 */
	public Enemic(Field f,int x1,int y1,int tamany,String path,int vida) {
		super("TheEnemy", x1, y1, (x1+tamany), (y1+tamany), 0, path, f);
		this.vida=vida;
		this.dormido=true;
	}

	public void danyar() {
		if(this.vida>1) {
			this.vida--;
			if(dormido) {
				this.path="resources/creatureA.gif";
				this.dormido=false;
			}
		}else {
			this.delete();
		}
	}
	
	@Override
	public void onCollisionEnter(Sprite spriteTocado) {
		
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
	
	

}
