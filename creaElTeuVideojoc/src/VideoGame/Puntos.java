package VideoGame;

import Core.Field;
import Core.Sprite;

public class Puntos extends Sprite{
	private int cicles;
	//Son el sprite de 100p que se ve al conseguir puntos
	public Puntos( int x1, int y1, int x2, int y2, Field f) {
		super("100p", ((x1+x2)/2-50), ((y1+y2)/2-180), ((x1+x2)/2+50), ((y1+y2)/2-120), 0, "resources/misc/100p.png", f);
		solid=false;
		cicles=35;
	}
	@Override
	public void update() {
		super.update();
		if(cicles==0) {
			this.delete(); 
		}else {
			cicles--;
		}
	}
	
}
