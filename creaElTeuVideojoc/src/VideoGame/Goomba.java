package VideoGame;

import Core.*;

public class Goomba extends PhysicBody implements Enemigos {

	public Goomba(Field f, int x1, int y1, int tamany) {
		super("TheEnemy", x1, y1, (x1 + tamany), (y1 + tamany), 0, "resources/enemies/goomba.gif", f);
		this.velocity[0] = -2;
		this.addConstantForce(0, 0.6);
	}

	public void danyar() {
		this.path = "resources/enemies/goombaH.png";
		Testing.arraytodo.add(new Puntos(x1, y1, x2, y2, f));
		Testing.puntuacio.sumarPuntos(100);
		this.delete();
	}

	@Override
	public void onCollisionEnter(Sprite spriteTocado) {
		if (spriteTocado.y1 < this.y2) {
			if (x2<=spriteTocado.x1) {
				this.velocity[0] = -2;
			} else {
				this.velocity[0] = 2;
			}
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

}
